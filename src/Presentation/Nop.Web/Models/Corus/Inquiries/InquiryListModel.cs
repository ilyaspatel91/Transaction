﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;
using Nop.Web.Models.News;

namespace Nop.Web.Models.Corus.Inquiries
{
    public partial class InquiryListModel : BaseNopModel
    {
        public InquiryListModel()
        {
            PagingFilteringContext = new InquiryPagingFilteringModel();
            Inquiries = new List<InquiryModel>();
            PagerModel = new PagerModel();
        }

        public InquiryPagingFilteringModel PagingFilteringContext { get; set; }
        public IList<InquiryModel> Inquiries { get; set; }

        public PagerModel PagerModel { get; set; }
    }
}