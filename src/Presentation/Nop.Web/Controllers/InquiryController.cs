﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Corus.Inquiries;
using Nop.Core.Domain.Gdpr;
using Nop.Services.Catalog;
using Nop.Services.Corus.Inquiries;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Gdpr;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Seo;
using Nop.Web.Factories;
using Nop.Web.Factories.Cours.Inquiries;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Models.Corus.Inquiries;

namespace Nop.Web.Controllers
{

    public partial class InquiryController : BasePublicController
    {
        #region Fields


        private readonly GdprSettings _gdprSettings;
        private readonly ICountryService _countryService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ICustomerModelFactory _customerModelFactory;
        private readonly ICustomerService _customerService;
        private readonly IGdprService _gdprService;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IWorkContext _workContext;
        private readonly IInquiryService _inquiryService;
        private readonly INotificationService _notificationService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IInquiryModelFactory _inquiryModelFactory;

        #endregion

        #region Ctor

        public InquiryController(
            GdprSettings gdprSettings,
            ICountryService countryService,
            ICustomerAttributeParser customerAttributeParser,
            ICustomerAttributeService customerAttributeService,
            ICustomerModelFactory customerModelFactory,
            ICustomerService customerService,
            IGdprService gdprService,
            ILocalizationService localizationService,
            ILogger logger,
            IStateProvinceService stateProvinceService,
            IWorkContext workContext,
            IInquiryService inquiryService,
            INotificationService notificationService,
            IProductService productService,
            IUrlRecordService urlRecordService ,
            IInquiryModelFactory inquiryModelFactory)
        {

            _gdprSettings = gdprSettings;
            _countryService = countryService;
            _customerAttributeParser = customerAttributeParser;
            _customerAttributeService = customerAttributeService;
            _customerModelFactory = customerModelFactory;
            _customerService = customerService;
            _gdprService = gdprService;
            _localizationService = localizationService;
            _logger = logger;
            _stateProvinceService = stateProvinceService;
            _workContext = workContext;
            _inquiryService = inquiryService;
            _notificationService = notificationService;
            _productService = productService;
            _urlRecordService = urlRecordService;
            _inquiryModelFactory = inquiryModelFactory;
        }

        #endregion

        #region Utilities

        #endregion

        #region Methods

        #region Inquiry

        [HttpsRequirement]
        public virtual IActionResult CustomerInquiry()
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            return View();
        }

        [HttpPost]
        public IActionResult DataSource(DataTableAjaxPostModel ajaxModel)
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            // action inside a standard controller
            //var res = _inquiryService.GetAllInquiry(keyword: ajaxModel.Search.value, customerId: _workContext.CurrentCustomer.Id, pageIndex:(ajaxModel.Start/ajaxModel.Length), pageSize: ajaxModel.Length);

            var listModel = _inquiryModelFactory.PrepareAjaxInquiryListModel(ajaxModel);
            return Json(new
            {
                // this is what datatables wants sending back
                draw = ajaxModel.Draw,
                recordsTotal = listModel.PagerModel.TotalRecords,
                recordsFiltered = listModel.PagerModel.TotalRecords,
                data = listModel.Inquiries
            });
        }

        [HttpPost]
        public virtual IActionResult CustomerInquiry(InquiryModel model)
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            model.CustomerId = _workContext.CurrentCustomer.Id;

            if (ModelState.IsValid)
            {
                var inqury = new Inquiry()
                {
                    ProductId = model.ProductId,
                    CustomerId = model.CustomerId,
                    InqProductAttributesXml = model.InqProductAttributesXml,
                    Quantity = model.Quantity,
                    PriceInclTax = model.PriceInclTax,
                    PriceExclTax = model.PriceExclTax,
                    InqStatusId = (int)InqStatus.Pending,
                    Description = model.Description,
                    InqDate = model.InqDate,
                    InqValidDate = model.InqValidDate,
                    CreatedOnUtc = DateTime.UtcNow
                };

                _inquiryService.InsertInquiry(inqury);

                //Success notification
                _notificationService.SuccessNotification(_localizationService.GetResource("Corus.Customer.Product.Inquiry.AddNew"));
                var product = _productService.GetProductById(model.ProductId);

                var productSename = _urlRecordService.GetSeName(product);

                return RedirectToRoute("Product", new { SeName = productSename });
            }

            return View(model);
        }


        [HttpsRequirement]
        public virtual IActionResult CustomerInquiryList(int? pageNumber)
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            var model = _inquiryModelFactory.PrepareCustomerInquiryListModel(pageNumber);
            return View(model);
        }

        public virtual IActionResult CustomerInquiryDetail(int inquiryId)
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            var model = new InquiryModel();

            if (inquiryId > 0)
            {
                var inquiry = _inquiryService.GetInquiryById(inquiryId);
                if (inquiry != null)
                {
                    model = _inquiryModelFactory.PrepareInquiryModel(model, inquiry);
                    return View("~/Views/Inquiry/InquiryDetail.cshtml", model);
                }
            }

            return View("~/Views/Inquiry/InquiryDetail.cshtml", model);
        }

        [HttpPost]
        public virtual IActionResult CustomerInquiryDetail(InquiryModel model)
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    var inquiry = _inquiryService.GetInquiryById(model.Id);
                    if (inquiry != null)
                    {
                        inquiry.Id = model.Id;
                        inquiry.ProductId = model.ProductId;
                        inquiry.CustomerId = _workContext.CurrentCustomer.Id;
                        inquiry.InqProductAttributesXml = model.InqProductAttributesXml;
                        inquiry.Quantity = model.Quantity;
                        inquiry.PriceInclTax = model.PriceInclTax;
                        inquiry.PriceExclTax = model.PriceExclTax;
                        inquiry.InqStatusId = model.InqStatusId;
                        inquiry.Description = model.Description;
                        inquiry.InqDate = model.InqDate;
                        inquiry.InqValidDate = model.InqValidDate;
                        inquiry.CreatedOnUtc = model.CreatedOnUtc;
                        inquiry.UpdatedOnUtc = model.UpdatedOnUtc;

                        _inquiryService.UpdateInquiry(inquiry);
                    }

                    return RedirectToAction("CustomerInquiryDetail", new { inquiryId = model.Id });
                }
            }
            return Challenge();
        }

        #endregion

        #region Custom Button Inquiry

        //public ActionResult RequestInquiry(int productId)
        //{
        //    if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
        //        return Challenge();

        //    QuotationModel quotationModel = _quotationModelFactory.PrepareCreateModel(productId);
        //    var quotation = _quotationService.GetByCustomerIdProductId(_workContext.CurrentCustomer.Id, productId);
        //    if (quotation != null && quotation.Status != (int)QuotationStatus.VendorRefuse && quotation.Status != (int)QuotationStatus.VendorAccept)
        //    {
        //        quotationModel.Result = _localizationService.GetResource("Quotation.RequestQuotation.RequestQuotationAlreadySended");
        //        quotationModel.AlreadyExist = true;
        //    }

        //    return View(quotationModel);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult RequestInquiry(QuotationModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        decimal unityPriceWithStoreCurrency = _priceCalculationService.GetFinalPrice(_productService.GetProductById(model.ProductId), _workContext.CurrentCustomer, includeDiscounts: false);
        //        bool hasBlackListTerms = _settingSetvice.GetSettingByKey<string>("blacklisttext").Contains(model.Specification);
        //        Quotation quotation = new Quotation
        //        {
        //            CountryId = model.CountryId,
        //            CreationDate = DateTime.Now,
        //            CustomerId = _workContext.CurrentCustomer.Id,
        //            LeadTime = model.LeadTime,
        //            Price = _priceCalculationService.GetPriceWithFees(unityPriceWithStoreCurrency, (int)model.Quantity),
        //            ProductId = model.ProductId,
        //            Quantity = model.Quantity,
        //            Specification = model.Specification,
        //            Status = hasBlackListTerms ? (int)QuotationStatus.ModerationPending : (int)QuotationStatus.CustomerSend,//TODO Moderation
        //            UnitPrice = model.UnityPrice,
        //            StatusDate = DateTime.Now,
        //            VendorId = model.VendorId

        //        };
        //        _quotationService.Save(quotation);
        //        if (quotation != null && quotation.Id > 0)
        //        {
        //            model.Result = _localizationService.GetResource("Quotation.Request.Success");
        //            model.AlreadyExist = true;
        //            QuotationMessage quotationMessage = new QuotationMessage
        //            {
        //                Id = quotation.Id,
        //                CustomerId = quotation.CustomerId,
        //                ProductId = quotation.ProductId,
        //                VendorId = quotation.VendorId
        //            };
        //            //send request quote customer notification
        //            _workflowMessageService.SendRequestQuoteCustomerNotification(quotationMessage, _workContext.WorkingLanguage.Id);

        //            //send request quote vendor notification
        //            _workflowMessageService.SendRequestQuoteVendorNotification(quotationMessage, _workContext.WorkingLanguage.Id);
        //        }
        //    }
        //    return View(_quotationModelFactory.PrepareCreateModel(model.ProductId));
        //}

        #endregion
        #endregion
    }
}