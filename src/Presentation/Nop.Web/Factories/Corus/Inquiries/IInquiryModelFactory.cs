﻿﻿using Nop.Core.Domain.Corus.Inquiries;
using Nop.Web.Models.Corus.Inquiries;

namespace Nop.Web.Factories.Cours.Inquiries
{
    public partial interface IInquiryModelFactory
    {
        InquiryModel PrepareInquiryModel(InquiryModel model, Inquiry inquiry);

        InquiryListModel PrepareCustomerInquiryListModel(int? page);

        InquiryListModel PrepareAjaxInquiryListModel(DataTableAjaxPostModel model);
    }
}