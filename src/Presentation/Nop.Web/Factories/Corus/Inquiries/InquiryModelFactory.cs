﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Corus.Inquiries;
using Nop.Services.Catalog;
using Nop.Services.Corus.Inquiries;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Vendors;
using Nop.Web.Models.Common;
using Nop.Web.Models.Corus.Inquiries;

namespace Nop.Web.Factories.Cours.Inquiries
{

    public partial class InquiryModelFactory : IInquiryModelFactory
    {
        #region Fields

        private readonly IInquiryService _inquiryService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IVendorService _vendorService;
        private readonly IProductService _productService;
        #endregion

        #region Ctor

        public InquiryModelFactory(IInquiryService inquiryService, IWorkContext workContext, IStoreContext storeContext, ILocalizationService localizationService, ICustomerService customerService, IVendorService vendorService, IProductService productService)
        {
            _inquiryService = inquiryService;
            _workContext = workContext;
            _storeContext = storeContext;
            _localizationService = localizationService;
            _customerService = customerService;
            _vendorService = vendorService;
            _productService = productService;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Prepare the customer reward points model
        /// </summary>
        /// <param name="page">Number of items page; pass null to load the first page</param>
        /// <returns>Customer reward points model</returns>
        public virtual InquiryListModel PrepareCustomerInquiryListModel(int? page)
        {
            //get reward points history
            var customer = _workContext.CurrentCustomer;
            var store = _storeContext.CurrentStore;
            var pageSize = 10;
            var inquiries = _inquiryService.GetAllInquiry(customerId: _workContext.CurrentCustomer.Id, pageIndex: --page ?? 0, pageSize: pageSize);

            //prepare model
            var model = new InquiryListModel
            {
                Inquiries = inquiries.Select(inquiry =>
                {
                    return new InquiryModel
                    {
                        Id = inquiry.Id,
                        ProductId = inquiry.ProductId,
                        CustomerId = inquiry.CustomerId,
                        InqProductAttributesXml = inquiry.InqProductAttributesXml,
                        Quantity = inquiry.Quantity,
                        PriceInclTax = inquiry.PriceInclTax,
                        PriceExclTax = inquiry.PriceExclTax,
                        InqStatusId = inquiry.InqStatusId,
                        Description = inquiry.Description,
                        InqDate = inquiry.InqDate,
                        InqValidDate = inquiry.InqValidDate,
                        CreatedOnUtc = inquiry.CreatedOnUtc,
                        UpdatedOnUtc = inquiry.UpdatedOnUtc,
                    };
                }).ToList(),

                PagerModel = new PagerModel
                {
                    PageSize = inquiries.PageSize,
                    TotalRecords = inquiries.TotalCount,
                    PageIndex = inquiries.PageIndex,
                    ShowTotalSummary = true,
                    RouteActionName = "CustomerInquiryList",
                    UseRouteLinks = true,
                    RouteValues = new RewardPointsRouteValues { pageNumber = page ?? 0 }
                }
            };

            return model;
        }


        public virtual InquiryListModel PrepareAjaxInquiryListModel(DataTableAjaxPostModel model)
        {
            var inquiries = _inquiryService.GetAllInquiry(keyword: model.Search.value, customerId: _workContext.CurrentCustomer.Id, pageIndex: (model.Start / model.Length), pageSize: model.Length);

            //prepare model
            return new InquiryListModel
            {
                Inquiries = inquiries.Select(inquiry =>
                {
                    var customer = (_workContext.CurrentCustomer.Id == inquiry.CustomerId) ? _workContext.CurrentCustomer : _customerService.GetCustomerById(inquiry.CustomerId);
                    var vendor = _vendorService.GetVendorByProductId(inquiry.ProductId)?.Name;

                    return new InquiryModel
                    {
                        Id = inquiry.Id,
                        ProductId = inquiry.ProductId,
                        ProductName = _localizationService.GetLocalized(_productService.GetProductById(inquiry.ProductId), x => x.Name),
                            VendorName = _localizationService.GetLocalized(_vendorService.GetVendorByProductId(inquiry.ProductId), x => x.Name),
                        //ProductName = _productService.GetProductById(inquiry.ProductId)?.Name,
                        //VendorName = _vendorService.GetVendorByProductId(inquiry.ProductId)?.Name ?? string.Empty,
                        CustomerId = inquiry.CustomerId,
                        CustomerName = _customerService.GetCustomerFullName(customer),
                        //InqProductAttributesXml = inquiry.InqProductAttributesXml,
                        Quantity = inquiry.Quantity,
                        PriceInclTax = inquiry.PriceInclTax,
                        PriceExclTax = inquiry.PriceExclTax,
                        InqStatusId = inquiry.InqStatusId,
                        //DispInqStatus = _localizationService.GetLocalizedEnum(inquiry.InqStatus),
                        Description = inquiry.Description,
                        InqDate = inquiry.InqDate,
                        DispInqDate = inquiry.InqDate.ToString("dd/MM/yyyy"),
                        InqValidDate = inquiry.InqValidDate,
                        DispInqValidDate = inquiry.InqValidDate.ToString("dd/MM/yyyy"),
                        CreatedOnUtc = inquiry.CreatedOnUtc,
                        UpdatedOnUtc = inquiry.UpdatedOnUtc,
                    };
                }).ToList(),

                PagerModel = new PagerModel
                {
                    PageSize = inquiries.PageSize,
                    TotalRecords = inquiries.TotalCount,
                    PageIndex = inquiries.PageIndex,
                }
            };                                                                                          
        }

        public virtual InquiryModel PrepareInquiryModel(InquiryModel model, Inquiry inquiry)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (inquiry == null)
                throw new ArgumentNullException(nameof(inquiry));

            model.Id = inquiry.Id;
            model.ProductId = inquiry.ProductId;
            model.CustomerId = inquiry.CustomerId;
            model.InqProductAttributesXml = inquiry.InqProductAttributesXml;
            model.Quantity = inquiry.Quantity;
            model.PriceInclTax = inquiry.PriceInclTax;
            model.PriceExclTax = inquiry.PriceExclTax;
            model.InqStatusId = inquiry.InqStatusId;
            model.Description = inquiry.Description;
            model.InqDate = inquiry.InqDate;
            model.InqValidDate = inquiry.InqValidDate;
            model.CreatedOnUtc = inquiry.CreatedOnUtc;
            model.UpdatedOnUtc = inquiry.UpdatedOnUtc;

            return model;
        }



        #endregion
    }
}