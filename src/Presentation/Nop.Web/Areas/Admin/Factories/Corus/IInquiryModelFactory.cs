﻿using Nop.Web.Areas.Admin.Models.Inquiries;

namespace Nop.Web.Areas.Admin.Factories.Cours
{
    public interface IInquiryModelFactory
    {
        /// <summary>
        /// Prepare inquiry search model
        /// </summary>
        /// <param name="searchModel">Inquiry search model</param>
        /// <returns>Inquiry search model</returns>
        InquirySearchModel PrepareInquirySearchModel(InquirySearchModel searchModel);

        /// <summary>
        /// Prepare paged inquiry list model
        /// </summary>
        /// <param name="searchModel">Inquiry search model</param>
        /// <returns>Inquiry list model</returns>
        InquiryListModel PrepareInquiryListModel(InquirySearchModel searchModel);
    }
}
