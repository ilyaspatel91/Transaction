﻿using System;
using System.Linq;
using Nop.Services.Catalog;
using Nop.Services.Corus.Inquiries;
using Nop.Services.Customers;
using Nop.Web.Areas.Admin.Models.Inquiries;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories.Cours
{
    public class InquiryModelFactory : IInquiryModelFactory
    {
        private readonly IInquiryService _inquiryService;
        private readonly ICustomerService _customerService;
        private readonly IProductService _productService;

        public InquiryModelFactory(IInquiryService inquiryService,
            ICustomerService customerService,
            IProductService productService)
        {
            _inquiryService = inquiryService;
            _customerService = customerService;
            _productService = productService;
        }

        /// <summary>
        /// Prepare inquiry search model
        /// </summary>
        /// <param name="searchModel">Inquiry search model</param>
        /// <returns>Inquiry search model</returns>
        public virtual InquirySearchModel PrepareInquirySearchModel(InquirySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged inquiry list model
        /// </summary>
        /// <param name="searchModel">Inquiry search model</param>
        /// <returns>Inquiry list model</returns>
        public virtual InquiryListModel PrepareInquiryListModel(InquirySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var getAllInquiry = _inquiryService.GetAllInquiry().ToPagedList(searchModel);

            //prepare list model
            var model = new InquiryListModel().PrepareToGrid(searchModel, getAllInquiry, () =>
            {
                return getAllInquiry.Select(inquiry =>
                {
                    var inq = new InquiryModel()
                    {
                        CustomerId = inquiry.CustomerId,
                        CustomerEmail = _customerService.GetCustomerById(inquiry.CustomerId)?.Email,
                        ProductId = inquiry.ProductId,
                        ProductName = _productService.GetProductById(inquiry.ProductId)?.Name,
                        Quantity = inquiry.Quantity,
                        InqDate = inquiry.InqDate

                    };
                    return inq;
                });
            });

            return model;
        }
    }
}
