﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Areas.Admin.Factories.Cours;
using Nop.Web.Areas.Admin.Models.Inquiries;

namespace Nop.Web.Areas.Admin.Controllers.Corus
{
    public class InquiryController : BaseAdminController
    {
        #region Fields

        private readonly IInquiryModelFactory _inquiryModelFactory;
       
        #endregion

        #region Ctor

        public InquiryController(IInquiryModelFactory storeModelFactory)
        {
            _inquiryModelFactory = storeModelFactory;
        }

        #endregion

        public virtual IActionResult List()
        {
            //prepare model
            var model = _inquiryModelFactory.PrepareInquirySearchModel(new InquirySearchModel());
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(InquirySearchModel searchModel)
        {
            //prepare model
            var model = _inquiryModelFactory.PrepareInquiryListModel(searchModel);

            return Json(model);
        }
    }
}
