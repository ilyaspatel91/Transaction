﻿using System;
using Nop.Core.Domain.Corus.Inquiries;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Inquiries
{
    public partial class InquiryModel : BaseNopEntityModel
    {        
        public int CustomerId { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.CustomerEmail")]
        public string CustomerEmail { get; set; }

        public int ProductId { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.ProductName")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.InqProductAttributesXml")]
        public string InqProductAttributesXml { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.Quantity")]
        public int Quantity { get; set; }
        
        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.PriceInclTax")]
        public decimal PriceInclTax { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.PriceExclTax")]
        public decimal PriceExclTax { get; set; }
        public int InqStatusId { get; set; }
        public InqStatus InqStatus { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.InqDate")]
        public DateTime InqDate { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.InqValidDate")]
        public DateTime InqValidDate { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
    }
}