﻿using System;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Inquiries
{
    public class InquirySearchModel : BaseSearchModel
    {
        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.CustomerEmail")]
        public string CustomerEmail { get; set; }

        [NopResourceDisplayName("Corus.Customer.Product.Inquiry.Field.ProductName")]
        public string ProductName { get; set; }

        public DateTime InquiryStartDate{ get; set; }
        public DateTime InquiryEndDate { get; set; }
    }
}
