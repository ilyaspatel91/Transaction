﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Corus.Inquiries;

namespace Nop.Web.Validators.Corus.Inquiries
{
    public class InquiryModelValidator : BaseNopValidator<InquiryModel>
    {
        public InquiryModelValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.InqProductAttributesXml).NotEmpty().WithMessage(localizationService.GetResource("Corus.Customer.Product.Inquiry.Field.InqProductAttributesXml.Required"));
            RuleFor(x => x.Quantity).NotEqual(0).WithMessage(localizationService.GetResource("Corus.Customer.Product.Inquiry.Field.Quantity.Required"));
            RuleFor(x => x.InqDate).NotEmpty().WithMessage(localizationService.GetResource("Corus.Customer.Product.Inquiry.Field.InqDate.Required"));
            RuleFor(x => x.InqValidDate).NotEmpty().WithMessage(localizationService.GetResource("Corus.Customer.Product.Inquiry.Field.InqValidDate.Required"));
        }
    }
}
