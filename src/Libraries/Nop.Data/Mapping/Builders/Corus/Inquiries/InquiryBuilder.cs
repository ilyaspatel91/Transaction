﻿using System.Data;
using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Corus.Inquiries;
using Nop.Core.Domain.Customers;
using Nop.Data.Extensions;

namespace Nop.Data.Mapping.Builders.Corus.Inquiries
{
    public partial class InquiryBuilder : NopEntityBuilder<Inquiry>
    {
        #region Methods
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(Inquiry.CustomerId)).AsInt32().Nullable().ForeignKey<Customer>(onDelete: Rule.None)
                .WithColumn(nameof(Inquiry.ProductId)).AsInt32().NotNullable().ForeignKey<Product>(onDelete: Rule.None);
        }

        #endregion
    }
}