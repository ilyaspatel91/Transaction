﻿using System;

namespace Nop.Core.Domain.Corus.Inquiries
{
    public partial class Inquiry : BaseEntity
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public string InqProductAttributesXml { get; set; }
        public int Quantity { get; set; }
        public decimal PriceInclTax { get; set; }
        public decimal PriceExclTax { get; set; }
        public int InqStatusId { get; set; }
        public InqStatus InqStatus
        {
            get => (InqStatus)InqStatusId;
            set => InqStatusId = (int)value;

        }
        public string Description { get; set; }
        public DateTime InqDate { get; set; }
        public DateTime InqValidDate { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
    }
}
