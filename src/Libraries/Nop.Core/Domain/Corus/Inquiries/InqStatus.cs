﻿namespace Nop.Core.Domain.Corus.Inquiries
{
    public enum InqStatus
    {
        Pending = 10,

        Processing = 20,

        Complete = 30,

        Cancelled = 40,

        Rejected = 50,

    }
}
