﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Corus.Inquiries;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;

namespace Nop.Services.Corus.Inquiries
{
    public partial class InquiryService : IInquiryService
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Inquiry> _inquiryRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;

        #endregion

        #region Ctor

        public InquiryService(CatalogSettings catalogSettings,
            ICacheKeyService cacheKeyService,
            IEventPublisher eventPublisher,
            IRepository<StoreMapping> storeMappingRepository,
            IRepository<Inquiry> inquiryRepository, 
            IRepository<Product> productRepository, 
            IRepository<Customer> customerRepository)
        {
            _catalogSettings = catalogSettings;
            _cacheKeyService = cacheKeyService;
            _eventPublisher = eventPublisher;
            _storeMappingRepository = storeMappingRepository;
            _inquiryRepository = inquiryRepository;
            _productRepository = productRepository;
            _customerRepository = customerRepository;
        }

        #endregion

        #region Methods

        #region Inquiry
        public virtual void DeleteInquiry(Inquiry inquiry)
        {
            if (inquiry == null)
                throw new ArgumentNullException(nameof(inquiry));

            _inquiryRepository.Delete(inquiry);

            //event notification
            _eventPublisher.EntityDeleted(inquiry);
        }

        public virtual Inquiry GetInquiryById(int inquiryId)
        {
            if (inquiryId == 0)
                return null;

            return _inquiryRepository.ToCachedGetById(inquiryId);
        }

        public virtual IList<Inquiry> GetInquiryByIds(int[] inquiryIds)
        {
            var query = _inquiryRepository.Table;
            return query.Where(p => inquiryIds.Contains(p.Id)).ToList();
        }

        public virtual IPagedList<Inquiry> GetAllInquiry(string keyword = "", int customerId = 0, int vendorId = 0, int statusId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, DateTime? createdFromUtc = null, DateTime? createdToUtc = null)
        {
            var query = _inquiryRepository.Table;
            if (customerId > 0)
                query = query.Where(n => customerId == n.CustomerId);

            if (vendorId > 0)
            {
                query = from inq in query
                        join p in _productRepository.Table on inq.ProductId equals p.Id
                        where p.VendorId == vendorId
                        select inq;
            }

            if (statusId > 0)
                query = query.Where(x => x.InqStatusId == statusId);

            if (!string.IsNullOrEmpty(keyword))
            {
                query = from inq in query
                        join p in _productRepository.Table on inq.ProductId equals p.Id
                        join c in _customerRepository.Table on inq.CustomerId equals c.Id
                        where keyword.Contains(p.Name) || keyword.Contains(c.Email)
                        select inq;
            }

            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);

            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);

            query = query.OrderByDescending(n => n.Id);

            var inqueries = new PagedList<Inquiry>(query, pageIndex, pageSize);

            return inqueries;
        }

        public virtual void InsertInquiry(Inquiry inquiry)
        {
            if (inquiry == null)
                throw new ArgumentNullException(nameof(inquiry));

            _inquiryRepository.Insert(inquiry);

            //event notification
            _eventPublisher.EntityInserted(inquiry);
        }

        public virtual void UpdateInquiry(Inquiry inquiry)
        {
            if (inquiry == null)
                throw new ArgumentNullException(nameof(inquiry));

            _inquiryRepository.Update(inquiry);

            //event notification
            _eventPublisher.EntityUpdated(inquiry);
        }

        public virtual IList<Inquiry> GetAllInquiry()
        {
            return _inquiryRepository.Table.ToList();
        }
        #endregion

        #endregion
    }
}