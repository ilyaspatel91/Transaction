﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Corus.Inquiries;

namespace Nop.Services.Corus.Inquiries
{
    public partial interface IInquiryService
    {
        #region Inquiry

        void DeleteInquiry(Inquiry inquiry);

        Inquiry GetInquiryById(int inquiryId);


        IList<Inquiry> GetInquiryByIds(int[] inquiryIds);

        IPagedList<Inquiry> GetAllInquiry(string keyword = "", int customerId = 0, int vendorId = 0, int statusId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, DateTime? createdFromUtc = null, DateTime? createdToUtc = null);

        void InsertInquiry(Inquiry inquiry);

        void UpdateInquiry(Inquiry inquiry);

        IList<Inquiry> GetAllInquiry();

        #endregion
    }
}