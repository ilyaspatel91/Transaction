﻿using Nop.Core.Caching;

namespace Nop.Services.Corus.Inquiries
{
    public static partial class InquiryDefaults
    {
        #region Caching defaults

        public static CacheKey InquiryCacheKey => new CacheKey("Nop.inquiry-{0}-{1}-{2}", InquiryCacheKeyPrefixCacheKey);
         
        public static string InquiryCacheKeyPrefixCacheKey => "Nop.inquiry-{0}";

        #endregion
    }
}