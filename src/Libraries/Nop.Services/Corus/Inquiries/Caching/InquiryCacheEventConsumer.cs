﻿using Nop.Core.Domain.Corus.Inquiries;
using Nop.Core.Domain.News;
using Nop.Services.Caching;
using Nop.Services.Corus.Inquiries;

namespace Nop.Services.Corus.Inquiries.Caching
{
    public partial class InquiryCacheEventConsumer : CacheEventConsumer<Inquiry>
    {
        /// <summary>
        /// Clear cache data
        /// </summary>
        /// <param name="entity">Entity</param>
        protected override void ClearCache(Inquiry entity)
        {
            var prefix = _cacheKeyService.PrepareKeyPrefix(InquiryDefaults.InquiryCacheKeyPrefixCacheKey, entity);

            RemoveByPrefix(prefix);
        }
    }
}